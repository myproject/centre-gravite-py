import time
import pygame

WINDOWSIZE = (500, 500)

pygame.init()
fenetre = pygame.display.set_mode(WINDOWSIZE)
pygame.display.set_caption("Center Gravity Point")

time_per_frame = 1 / 60
last_time = time.time()
frameCount = 0

pointList = []

run = True
while run:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                run = False
            elif keys[pygame.K_F5]:
                pointList = []

            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                pointList.append(pos)

        fenetre.fill((0,0,0))

        allX = 0
        allY = 0
        for i in range(0, len(pointList)):
            allX += pointList[i][0]
            allY += pointList[i][1]

            pygame.draw.circle(fenetre, (255,255,255), pointList[i], 2)
            if i != len(pointList) -1:
                pygame.draw.line(fenetre, (255,255,255), pointList[i], pointList[i + 1], 1)
            else:
                pygame.draw.line(fenetre, (255, 255, 255), pointList[i], pointList[0], 1)

        if len(pointList) > 1:
            pygame.draw.circle(fenetre, (0,255,0), (allX / len(pointList), allY / len(pointList)), 2)

    pygame.display.update()

    last_time = time.time()
    frameCount += 1
